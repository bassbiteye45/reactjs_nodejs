const express = require("express");
const upload = require("express-fileupload");
const bodyParser = require("body-parser");
var cors = require("cors");

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var fs = require("fs");
app.use(upload());
app.use(cors());
app.get("/", (req, res) => {
  fs.readFile("filetoedit.js", function (_err, data) {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write(data);
    return res.end();
  });
});

app.post("/save", (req, res) => {
  fs.writeFile("filetoedit.js", req.body.data, function (err) {
    if (err) {
      res.end("server error");
    } else {
      res.end("File updated successfully");
    }
  });
});
const port = 5000;

app.listen(port, () => `Server running on port ${port}`);
