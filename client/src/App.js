import React, { Component } from 'react'
import './App.css'
import { Button,Input, Space, Card, message } from 'antd'
import axios from 'axios'
import 'antd/dist/antd.css';

const { TextArea } = Input
class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: '',
      data: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange (event) {
    this.setState({ data: event.target.value })
  }

  handleSubmit (event) {
    event.preventDefault()
    const formData = new FormData() // formdata object
    const url = 'http://localhost:5000/save'
    formData.append('data', this.state.data) // append the values with key, value pair

    const config = {
      headers: { 'content-type': 'multipart/form-data' }
    }
    axios
      .post(url, formData, config)
      .then((response) => {
        console.log(response)
        message.success(response.data);
      })
      .catch((error) => {
        console.log(error)
      })
  }

  getData () {
    fetch('http://localhost:5000')
      .then((response) => response.text())
      .then((response) => this.setState({ data: response }))
  }

  componentDidMount () {
    this.getData()
  }

  render () {
    return (
      <div className='App'>
          <Card
            style={{ marginTop: '10px',marginLeft: '520px ', width: 300 }}
          >
          <p>/filetoedit.js</p>
          </Card>
        <form >
          <div style={{ margin: '24px 0' }} />
          <TextArea
            type='text'
            value={this.state.data}
            onChange={this.handleChange}
            style={{ width: '400px' }}
            autoSize={{ minRows: 10, maxRows: 10 }}
          />
          <div style={{ margin: '20px 0' }} />
          <Button style={{ marginLeft: '340px ' }} type='primary' onClick={this.handleSubmit}>
            save
          </Button>
        </form>
      </div>
    )
  }
}
export default App
